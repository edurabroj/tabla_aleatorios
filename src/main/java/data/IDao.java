package data;
import java.io.Serializable;
import java.util.List;

public interface IDao { 
	public void guardar(Object entidad);	
	public <T> void eliminar(Class<T> tabla, Serializable id);
	public void eliminarTodos(String tabla);
	public <T> List<T> obtenerTodos(Class<T> tabla);
	public <T> T obtenerPorId(Class<T> tabla, Serializable id);
	public <T> List<T> consultar(String consultaHql);
}
