<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Clientes</title>
</head>
<body>
	<h1>Lista de clientes</h1>
	
	<div>
   		<a href="<c:url value="/cliente/agregar"/>" class="btn btn-success">Agregar</a>
   	</div><br>
	
	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Ap. Paterno</th>
				<th>Ap. Materno</th>
				<th>Cumpleaņos</th>
				<th>Acciones</th>
			</tr>
		</thead>				
		<tbody>			
			<c:forEach var = "item" items="${lista}">
				<tr>
					<td><c:out value = "${item.id}"/></td>
					<td><c:out value = "${item.nombre}"/></td>
					<td><c:out value = "${item.aPaterno}"/></td>
					<td><c:out value = "${item.aMaterno}"/></td>
					<td><c:out value = "${item.fechaNac}"/></td>
					<td>
						<div class="col-sm-6">
							<a href="<c:url value="/cliente/editar?id=${item.id}"/>">Editar</a>
						</div>
   						    			
						<div class="col-sm-6">									
							<a href="<c:url value="/cliente/eliminar?id=${item.id}"/>">Eliminar</a>
						</div>
					</td>
				</tr>
	      	</c:forEach>
		</tbody>
	</table>	
	
</body>
</html>