package com.solweb.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import entities.Cliente;
import entities.Desastre;
import data.IDao;
import validators.ClienteValidator;
import validators.DesastreValidator;
import viewModels.ClienteMes;

@Controller
@RequestMapping("cliente")
public class ClienteController {
	private String carpetaVistas = "cliente/";
	private String redirectUrl = "redirect:/cliente";
	
	private HashMap<Integer, String> dict;
	
	public ClienteController() {
		dict = new HashMap<Integer,String>();
		dict.put(1, "Enero");
		dict.put(2, "Febrero");
		dict.put(3, "Marzo");
		dict.put(4, "Abril");
		dict.put(5, "Mayo");
		dict.put(6, "Junio");
		dict.put(7, "Julio");
		dict.put(8, "Agosto");
		dict.put(9, "Setiembre");
		dict.put(10, "Octubre");
		dict.put(11, "Noviembre");
		dict.put(12, "Diciembre");
	}
	
	@Autowired
	private IDao dao;
	
	@Autowired
	private ClienteValidator clienteValidator;
	
	@RequestMapping("")
	public ModelAndView index(HttpServletRequest req) {	
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "index");
		//List<Cliente> listaCliente = dao.obtenerTodos(Cliente.class);
		List<Cliente> listaCliente = dao.consultar("from Cliente d order by d.aPaterno desc, d.aMaterno desc");	
		mav.addObject("lista",listaCliente);
		return mav;
	}
	
	@RequestMapping("porMes")
	public ModelAndView porMes(HttpServletRequest req) 
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "porMes");
		List<Cliente> listaClientes = dao.obtenerTodos(Cliente.class);

		List<ClienteMes> lista = new ArrayList<ClienteMes>();
		
		for(int i=1; i<=12; i++) {
			lista.add( getClientesByMes(listaClientes,i));
		}
		
		mav.addObject("lista", lista);
		return mav;
	}
	
	private ClienteMes getClientesByMes(List<Cliente> listaClientes, int i) {
		int nro=0;
		for(Cliente c: listaClientes) {
			if(c.getFechaNac().getMonth()+1==i) {
				nro++;
			}
		}
		return new ClienteMes( dict.get(i) ,nro);
	}
	
	@RequestMapping("/agregar")
	public ModelAndView agregar(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "form");
		mav.addObject("objeto", new Cliente());
		return mav;
	}
	
	@RequestMapping(value="/agregar", method=RequestMethod.POST)
	public ModelAndView agregar
		(
				@ModelAttribute ("objeto") Cliente objeto,
				BindingResult result
		)
	{
		clienteValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(carpetaVistas + "form");
			mav.addObject("objeto", objeto);
			return mav;		
		}else {
			dao.guardar(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.GET)
	public ModelAndView editar(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "form");
		
		int id = Integer.parseInt(request.getParameter("id"));
		mav.addObject("objeto", dao.obtenerPorId(Cliente.class, id));
		return mav;
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.POST)
	public ModelAndView editar
	(
		@ModelAttribute ("objeto") Cliente objeto,
		BindingResult result
	)
	{
		clienteValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(carpetaVistas + "form");
			mav.addObject("objeto", objeto);
			return mav;		
		}else {
			dao.guardar(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/eliminar", method=RequestMethod.GET)
	public ModelAndView eliminar(HttpServletRequest request)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		dao.eliminar(Cliente.class, id);
		return new ModelAndView(redirectUrl);
	}
}
