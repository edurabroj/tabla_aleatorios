package data;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class Dao extends HibernateDaoSupport implements IDao{	
	@Autowired
	public Dao(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
 
	@Transactional
	public void guardar(Object entidad) {
		getHibernateTemplate().saveOrUpdate(entidad);
	}
	
	@Transactional
	public <T> void eliminar(Class<T> tabla, Serializable id) {
		T entidad = obtenerPorId(tabla, id);
		getHibernateTemplate().delete(entidad);
	}
 
	@Transactional(readOnly = true)
	public <T> List<T> obtenerTodos(Class<T> tabla) {
		return getHibernateTemplate().loadAll(tabla);
	}
 
	@Transactional(readOnly = true)
	public <T> T obtenerPorId(Class<T> tabla, Serializable id) {
		return (T) getHibernateTemplate().get(tabla, id);
	}
 
	@Transactional(readOnly = true)
	public <T> List<T> consultar(String consultaHql) {
		return getHibernateTemplate().find(consultaHql);
	}

	@Override
	public void eliminarTodos(String tabla) {
		getSession().createQuery("DELETE FROM " + tabla).executeUpdate();
	}	

	/*
	@Transactional(readOnly = true)
	public <T> T get(Class<T> entityClass, Serializable id) {
		final T entity = (T)getHibernateTemplate().get(entityClass, id);
		return entity;
	}*/
}