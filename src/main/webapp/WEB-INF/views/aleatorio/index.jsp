<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Aleatorio</title>
</head>
<body>	
	<div>
   		<a href="<c:url value="/aleatorio/agregar"/>" class="btn btn-success">Agregar</a>
   	</div>
	<div>
   		<a href="<c:url value="/aleatorio/limpiar"/>" class="btn btn-success">Limpiar</a>
   	</div><br>
	
	<table>
		<thead>
			<tr>
				<th>N�mero</th>
			</tr>
		</thead>				
		<tbody>			
			<c:forEach var = "item" items="${lista}">
				<tr>
					<td><c:out value = "${item.numero}"/></td>
				</tr>
	      	</c:forEach>
		</tbody>
	</table>	
	
</body>
</html>