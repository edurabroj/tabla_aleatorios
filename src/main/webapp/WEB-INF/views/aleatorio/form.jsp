<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"/></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css"/>
<title>Aleatorio</title>
</head>
<body>
	<form:form method="post" commandName="objeto">			
			<div>
				<form:label path="numero" id="generado">N�mero aleatorio</form:label>
				<form:input path="numero" cssClass="form-control"/>
				<form:errors path="numero"></form:errors>
			</div> 	<br>		
			<div>
				<input id="btnGenerar" type="button" value="Generar" class="btn btn-success"/>
			</div>	<br>
			<div>
				<input type="submit" value="Guardar" class="btn btn-success"/>
			</div>		
		</form:form>
	<script>	
		$("#btnGenerar").on("click", function(){
			var generado = getRandomInt(0,100)
			$("#numero").val(generado)
		})
	
		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min)) + min
		}
	</script>
</body>
</html>