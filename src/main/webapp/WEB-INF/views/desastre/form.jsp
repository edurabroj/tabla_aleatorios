<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"/></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css"/>
<title>Desastres</title>
</head>
<body>
	<form:form method="post" commandName="objeto">		
			<div>
				<form:label path="fecha">Fecha</form:label>
				<fmt:formatDate value="${objeto.fecha}" var="dateString" pattern="MM/dd/yyyy" />
				<form:input path="fecha" value="${dateString}" cssClass="form-control datepicker"/>
				<form:errors path="fecha"></form:errors>
			</div> 
			<div>
				<form:label path="tipo">Tipo</form:label>
				<form:input path="tipo" cssClass="form-control"/>
				<form:errors path="tipo"></form:errors>
			</div> 	
			<div>
				<form:label path="distrito">Distrito</form:label>
				<form:input path="distrito" cssClass="form-control"/>
				<form:errors path="distrito"></form:errors>
			</div> 	
			<div>
				<form:label path="nroDamn">N�mero de damnificados</form:label>
				<form:input path="nroDamn" cssClass="form-control"/>
				<form:errors path="nroDamn"></form:errors>
			</div> 	<br>	
			<div>
				<input type="submit" value="Guardar" class="btn btn-success"/>
			</div>		
		</form:form>
		<script>
			$('.datepicker').datepicker({
			    format: 'mm/dd/yyyy'
			});
		</script>
</body>
</html>