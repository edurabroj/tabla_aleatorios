package com.solweb.hibernate;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import entities.Aleatorio;
import data.IDao;

@Controller
@RequestMapping("aleatorio")
public class AleatorioController {
	private String carpetaVistas = "aleatorio/";
	private String redirectUrl = "redirect:/aleatorio";
	
	@Autowired
	private IDao dao;
	
	@RequestMapping("")
	public ModelAndView index(HttpServletRequest req) {	
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "index");
		List<Aleatorio> listaAleatorios = dao.obtenerTodos(Aleatorio.class);
		mav.addObject("lista",listaAleatorios);
		return mav;
	}
	
	@RequestMapping("/agregar")
	public ModelAndView agregar(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "form");
		mav.addObject("objeto", new Aleatorio());
		return mav;
	}
	
	@RequestMapping(value="/agregar", method=RequestMethod.POST)
	public ModelAndView agregar
		(
				@ModelAttribute ("objeto") Aleatorio objeto,
				BindingResult result
		)
	{
		dao.guardar(objeto);
		return new ModelAndView(redirectUrl);
	}
	@RequestMapping(value="/limpiar")
	public ModelAndView limpiar()
	{
		dao.eliminarTodos("Aleatorio");;
		return new ModelAndView(redirectUrl);
	}
}
